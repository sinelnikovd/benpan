$(document).ready(function() {


  $(".partner .frame").sly({
    slidee: $(".partner .slidee"),
    horizontal: true,
    itemNav: 'basic',
    speed: 300,
    mouseDragging: true,
    touchDragging: true,
    prevPage: $(".partner .prev"),
    nextPage: $(".partner .next"),
    cycleBy: 'items',
    smart: true
  });

  
  $(".project-list .frame").sly({
    slidee: $(".project-list .slidee"),
    horizontal: true,
    itemNav: 'basic',
    speed: 300,
    mouseDragging: true,
    touchDragging: true,
    prevPage: $(".project-list .prev"),
    nextPage: $(".project-list .next"),
    cycleBy: 'items',
    smart: true
  });






  $(window).on('resize',function() {
    tabResize();
    if($('section').is(".top-sale")){
      slideResize();
    }
  });



  $(".overview__theme-nav a").on('click', function() {
    var $nav = $(this).closest(".overview__theme-nav");
    if(!$nav.hasClass("active")){
      $(".overview__theme-item.active").removeClass("active");
      $items.eq($nav.index()).addClass("active");

      $(".overview__theme-nav.active").removeClass("active");
      $nav.addClass("active");

      $(".overview .js-tab-height").height(itemsHeight[$nav.index()]);
      
    }
    return false;
  });

  function tabResize() {
    $items.each(function(index, el) {
      itemsHeight[index] = $(this).outerHeight();
    });

    $(".overview .js-tab-height").height(itemsHeight[$(".overview__theme-item.active").index()]);
  }




  var wrapWidth = 0;
  var wrapHeight = 0;
  function slideInit() {
    wrapWidth = $(window).width() - $(".top-sale__slider").offset().left;
    wrapWidth = (wrapWidth > 950) ? 950 : wrapWidth;
    wrapHeight = $(".top-sale").height();
    $(".top-sale__slider").width(wrapWidth);
    
    $(".top-sale__carousel-item").each(function(index, el) {
      $(this).height(wrapHeight);
    });
  }
  if($('section').is(".top-sale")){
    slideInit();
  }

  $(".top-sale .carousel").on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
      var i = (currentSlide ? currentSlide : 0) + 1;

      i = (i < 10) ? "0" + i : i;

      var count = (slick.slideCount < 10) ? "0" + slick.slideCount : slick.slideCount;

      $(".top-sale__count").html('<span>' + i + '</span>' + ' / ' + count);
  });

  $(".top-sale .carousel").slick({
    dots: false,
    arrows: true,
    prevArrow: $(".top-sale__prev"),
    nextArrow: $(".top-sale__next"),
    slidesToShow: 1,
    slidesToScroll: 1,
  });

  function slideResize() {
    wrapWidth = $(window).width() - $(".top-sale__slider").offset().left;
    wrapWidth = (wrapWidth > 950) ? 950 : wrapWidth;
    wrapHeight = $(".top-sale").height();
    $(".top-sale__slider").width(wrapWidth);
    
    $(".top-sale__carousel-item").each(function(index, el) {
      $(this).height(wrapHeight);
    });
  }




  $('#videbg').vide({
      mp4: 'video/bg.mp4',
      poster: 'video/bg.jpg'
    },{
      bgColor: "#11222d",
      posterType: "jpg"
    });

  $('#videbg2').vide({
      mp4: 'video/bg.mp4',
      poster: 'video/bg2.jpg'
    },{
      bgColor: "#1e214e",
      posterType: "jpg",
      className: 'vide-wrap'
    });

  $(".js-video-popup").magnificPopup({
    type: 'iframe',
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: 'v=',
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
      },
    }
  });

  $( ".accordion" ).accordion({
    collapsible:true,
    heightStyle:"content",
    icons: false
    });




  $(window).on('resize',function() {
    if (Modernizr.mq('(min-width: 992px)')) {
      var size = $(window).outerWidth() - $(".moreinfo .owl-stage-outer").offset().left;
      size = (size>1200) ? 1200 : size;
      $(".moreinfo .owl-stage-outer").css({
        "padding-right": size
      });
    }else{
      $(".moreinfo .owl-stage-outer").css({
        "padding-right": 0
      });
    }
  });

  $(".moreinfo .owl-carousel").owlCarousel({
    loop: true,
    nav: false,
    margin: 50,
    items: 1,
    responsive:{
        0:{
            items: 1
        },
        768:{
            items: 2
        },
        992:{
            items: 1
        }
    },
    onInitialized: function(event) {
      if (Modernizr.mq('(min-width: 992px)')) {
        var size = $(window).outerWidth() - $(this.$stage).closest(".owl-stage-outer").offset().left;
        size = (size>1200) ? 1200 : size;
        $(this.$stage).closest(".owl-stage-outer").css({
          "padding-right": size
        });
      }
    }
  });

  $(".moreinfo .arrow_prev").click(function() {
    $(".moreinfo .owl-carousel").trigger('prev.owl.carousel');
  });

  $(".moreinfo .arrow_next").click(function() {
    $(".moreinfo .owl-carousel").trigger('next.owl.carousel');
  });

});


var itemsHeight = [];
var $items = $(".overview__theme-item");
function tabInit () {
  $items.each(function() {
    itemsHeight.push($(this).outerHeight());
  });

  $items.wrapAll("<div class='js-tab-height'></div>");
  $(".overview .js-tab-height").height(itemsHeight[0]);
}

$(window).on('load', function() {
  tabInit();
});